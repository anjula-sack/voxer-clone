import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import 'firebase/storage';

const firebaseConfig = {
  apiKey: "AIzaSyAbRbOjiBH_WoBrqpyv5xdK--0icPPGzXo",
authDomain: "voxer-clone-1740a.firebaseapp.com",
projectId: "voxer-clone-1740a",
storageBucket: "voxer-clone-1740a.appspot.com",
messagingSenderId: "141937043359",
  appId: "1:141937043359:web:b3740e684ba90b2443ffaa"
}

firebase.initializeApp(firebaseConfig)

export const db = firebase.firestore();
export const auth = firebase.auth();
export const storage = firebase.storage();
