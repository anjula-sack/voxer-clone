import Vue from 'vue'
import App from './App.vue'
import { firestorePlugin } from 'vuefire'
import VueRouter from 'vue-router'
import VueCompositionApi from '@vue/composition-api'

import Home from './components/Home'
import ChatRoom from './components/ChatRoom'

Vue.config.productionTip = false

Vue.use(VueCompositionApi)
Vue.use(VueRouter)
Vue.use(firestorePlugin)

const router = new VueRouter({
  routes: [{path: '/', component: Home}, {path: '/chat/:id', component: ChatRoom, name: 'chat'}]
})

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
